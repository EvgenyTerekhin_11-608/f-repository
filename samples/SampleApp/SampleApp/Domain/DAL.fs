module SampleApp.Domain.DAL
open FSharp.Data.Sql.Common
open FSharp.Data.Sql
open System
open System.Security.Claims
open System.Threading
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Http.Features
open Microsoft.AspNetCore.Authentication
open Microsoft.AspNetCore.Authentication.Cookies
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open FSharp.Control.Tasks.V2.ContextInsensitive
open Giraffe
open Giraffe.HttpStatusCodeHandlers

open Giraffe.ModelValidation

open SampleApp.Models
open SampleApp.HtmlViews
//home/evgeny/Документы/f-repository/samples/SampleApp/SampleApp/

type IHasId =
    abstract id : int

type IHasErrorsValidator =
    abstract HasErrors : unit -> string option


[<CLIMutable>]
type DataStorage<'T when 'T :> IHasErrorsValidator and 'T :> IHasId> = 'T list


[<CLIMutable>]
type product =
    {
    id : int
    title : string
    price : int
    }
    interface IHasErrorsValidator with
       member this.HasErrors() =
         if this.id = 0 || this.title = null || this.price < 0 then Some "error" else None

    interface IModelValidation<product> with
        member this.Validate() =
            match (this :> IHasErrorsValidator).HasErrors() with
            | Some s -> Error(RequestErrors.badRequest (text s))
            | None -> Ok this;




let mutable products = [ { id = 1; title = "eda"; price = 100 }; { id = 2; title = "eda"; price = 100 }; { id = 3; title = "eda"; price = 100 } ]

let productCrud<'T when 'T :> IHasErrorsValidator and 'T :> IHasId> (datastorage : DataStorage<'T>) =
    let read id =
            let func next (ctx : HttpContext) =
                task {
                let product = datastorage |> List.tryFind (fun x -> x.id = id);
                return! match product with
                | Some s -> ctx.WriteJsonAsync s
                | None -> setStatusCode 400 next ctx
                }
            func

    let add =
        let func next (ctx : HttpContext) =
            task {
                let! product = ctx.BindJsonAsync<'T>();
                match product.HasErrors() with
                | Some s -> return! (setStatusCode 400 >=> json s) next ctx
                | None ->
                    datastorage <- product :: datastorage;
                    ctx.WriteJsonAsync product.id |> ignore
                    return Some ctx;
            }
        func

    let delete id =
        let func next (ctx : HttpContext) =
         task {
           datastorage <- datastorage |> List.filter (fun x -> x.id <> id)
           return Some ctx;
        }
        func

    let update =
        let func next (ctx : HttpContext) =
            task {
            let! newProduct = ctx.BindJsonAsync<'T>();
            datastorage <- newProduct :: (products |> List.filter (fun x -> x.id <> newProduct.id))
            return Some ctx
        }
        func

    choose
    [
        subRoute "/add" add
        subRoute "/update" update
        subRoute "/delete" delete
        subRoute "/read" read
    ]

